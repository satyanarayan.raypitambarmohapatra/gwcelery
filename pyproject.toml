[tool.poetry]
name = "gwcelery"
version = "0.0.0"
description = "Low-latency pipeline for annotating IGWN events"
readme = "README.rst"
authors = [
    "Deep Chatterjee <deep.chatterjee@ligo.org>",
    "Cody Messick <cody.messick@ligo.org>",
    "Geoffrey Mo <geoffrey.mo@ligo.org>",
    "Leo Singer <leo.singer@ligo.org>"
]
license = "GPL-2.0+"
classifiers = [
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)",
    "Operating System :: POSIX",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Topic :: Internet",
    "Topic :: Scientific/Engineering :: Astronomy",
    "Topic :: Scientific/Engineering :: Physics"
]
homepage = "https://git.ligo.org/emfollow/gwcelery"
repository = "https://git.ligo.org/emfollow/gwcelery"
documentation = "https://gwcelery.readthedocs.io/"
include = [
    "CHANGES.rst",
    "CONTRIBUTING.rst",
    "doc",
    "docs-requirements.txt",
    "gwcelery/static/*.css",
    "gwcelery/static/vega/*.html",
    "gwcelery/templates/*.jinja2",
    "gwcelery/data/*.sub",
    "gwcelery/data/first2years/*.xml.gz",
    "gwcelery/tests/data/*.html",
    "gwcelery/tests/data/*.json",
    "gwcelery/tests/data/*.xml",
    "gwcelery/tests/data/*.xml.gz",
    "gwcelery/tests/data/llhoft/*/*.gwf",
    "gwcelery/tests/data/llhoft/*/*/*.gwf",
    "gwcelery/tests/data/*.pickle",
    "gwcelery/tests/data/*.hdf5"
]

[tool.poetry.urls]
"Bug Tracker" = "https://git.ligo.org/emfollow/gwcelery/issues"

[tool.poetry.dependencies]
python = "^3.9,<3.11"
adc-streaming = ">=2.3.0"  # https://github.com/astronomy-commons/adc-streaming/pull/62
astropy = ">=4.3.1"  # https://github.com/astropy/astropy/issues/11879
bilby = ">=2.0.0"
bilby-pipe = ">=1.0.8"
celery = {version = ">=5.1", extras = ["redis"]}
click = ">=7"
comet = "*"
confluent-kafka = "^1.9.2"
dnspython = "*"  # silence "DNS: dnspython not found. Can not use SRV lookup." warning from SleekXMPP
flask = ">=2.2"
flask-caching = "*"
gracedb-sdk = ">=0.2.0"  # https://git.ligo.org/emfollow/gracedb-sdk/-/merge_requests/7
gwdatafind = ">=1.1.1"
gwpy = ">=2.0.1"  # https://github.com/gwpy/gwpy/issues/1277
healpy = "*"
hop-client = ">=0.7.0"  # https://github.com/scimma/hop-client/pull/176
igwn-alert = ">=0.2.2"
igwn-gwalert-schema = "^1.0.0"
imapclient = "*"
importlib-metadata = { version = "*"}
jinja2 = ">=2.11.2"  # https://github.com/pallets/jinja/issues/1168
lalsuite = ">=6.82"  # https://git.ligo.org/lscsoft/lalsuite/-/issues/414
ligo-followup-advocate = ">=1.1.6"
ligo-gracedb = ">=2.7.5"  # https://git.ligo.org/lscsoft/gracedb-client/-/issues/28
ligo-raven = "^2.0"
ligo-segments = "*"
"ligo.em-bright" = ">=1.1.1"  # https://git.ligo.org/emfollow/em-properties/em-bright/-/issues/23
"ligo.skymap" = ">=1.0.4"  # https://git.ligo.org/lscsoft/ligo.skymap/-/merge_requests/299
lscsoft-glue = "*"
lxml = "*"
matplotlib = "<3.7"  # Matplotlib changed an axes behaviour which breaks some of our plotting scripts. When gwpy has a new release, we can unpin this.
numba = ">=0.56" # Poetry update chooses an old version of numba and its deps that break the python3.9 and 3.10 build tests if this is not specified; dependence on numba comes from rift. Version chosen because it adds python 3.10 support. This requirement can be dropped here if RIFT adds it https://git.ligo.org/rapidpe-rift/rift/-/issues/24
numpy = "*"
p-astro = ">=1.0.1"  # https://git.ligo.org/lscsoft/p-astro/-/merge_requests/40
pesummary = "*"
pygcn = ">=1.0.1"
python-ligo-lw = "^1.8.3"
rapid-pe = ">=0.0.6" # https://git.ligo.org/computing/sccb/-/issues/1154
rapidpe-rift-pipe = ">=0.0.12" # https://git.ligo.org/computing/sccb/-/issues/1155
redis = "!=4.5.2,!=4.5.3" # https://git.ligo.org/emfollow/gwcelery/-/issues/556
RIFT = ">=0.0.15.7"
scipy = "<1.10"  # https://github.com/astropy/astropy/issues/14230
safe-netrc = "*"
sentry-sdk = {version = "*", extras = ["flask", "tornado"]}
service-identity = "*"  # We don't actually use this package, but it silences some annoying warnings from twistd.
voeventlib = ">=1.2"
werkzeug = ">=0.15.0"  # for werkzeug.middleware.proxy_fix.ProxyFix
zstandard = "*"  # for task compression

# For docs
pep517 = {version="*", optional=true}
sphinx = {version=">=4.0, <=5.3.0", optional=true}  # https://git.ligo.org/andrew.toivonen/gwcelery/-/jobs/2447152

# For tests
fastavro = {version = "^1.6.1", optional = true}
pytest-celery = {version="*", optional=true}
pytest-cov = {version="*", optional=true}
pytest-flask = {version="*", optional=true}
pytest-socket = {version="*", optional=true}

[tool.poetry.extras]
doc = ["pep517", "sphinx"]
test = ["fastavro", "pytest-celery", "pytest-cov", "pytest-flask", "pytest-socket"]

[tool.poetry.group.dev.dependencies]
ipython = "*"  # Include IPython for a nicer ``gwcelery shell`` experience.
ipykernel = "*"  # Include IPython kernel for Jupyter Lab support.
flower = ">=1.2.0"

[tool.poetry.scripts]
gwcelery = "gwcelery:main"
gwcelery-condor-submit-helper = "gwcelery.tools.condor_submit_helper:main"

[tool.poetry.plugins."celery.commands"]
condor = "gwcelery.tools.condor:condor"
flask = "gwcelery.tools.flask:flask"
nagios = "gwcelery.tools.nagios:nagios"

[tool.poetry-dynamic-versioning]
enable = true
bump = true

[tool.coverage.run]
source = ["gwcelery"]
omit = [
    "gwcelery/tests/*",
    "gwcelery/conf/development.py",
    "gwcelery/conf/playground.py",
    "gwcelery/conf/production.py",
    "gwcelery/conf/test.py"
]
parallel = true
concurrency = ["thread", "multiprocessing"]

[build-system]
requires = ["poetry_core>=1.0.0", "poetry-dynamic-versioning"]
build-backend = "poetry_dynamic_versioning.backend"
